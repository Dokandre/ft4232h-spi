CC=gcc
CFLAGS=-Wall -Wextra -L. -lftd2xx -Wl,-rpath /usr/local/lib

PROG = example
SRC = main.c ft_spi.c

all: $(PROG)

$(PROG): $(SRC)	
	$(CC) $(SRC) -pthread -g -o $(PROG) $(CFLAGS)	

clean:
	rm -f *.o ; rm *~; rm $(PROG)
